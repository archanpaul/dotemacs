;; Emacs configuration .emacs.d/init.el
;; paul . archan @ gmail.com

;; Install required packages
;; =========================

(require 'package)

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

(setq package-list '(
		     autopair
		     tabbar
		     neotree
		     exec-path-from-shell
		     python-environment
		     epc
		     jedi
		     virtualenvwrapper
		     pony-mode
		     go-mode
		     go-eldoc
		     neotree
		     tabbar
		     dart-mode
		     markdown-mode
		     company
		     company-go
		     company-web
		     company-tern
		     company-jedi
		     irony
		     company-irony
		     company-irony-c-headers
		     rtags
		     flycheck-rtags
		     flycheck-pyflakes
		     flymake-python-pyflakes
		     use-package
		     material-theme
		     ))
;; find : C-h v package-activated-list

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(dolist (package package-list)
  (when (not (package-installed-p package))
    (package-install package)))

;; Emacs window setup
;; ==================

(setq initial-frame-alist '((width . 100) (height . 52)))
(setq inhibit-startup-screen t)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-linum-mode t)
(column-number-mode t)

;; Emacs default behavior
;; ======================

(global-font-lock-mode 1) ; Enable syntax highlight

(global-hl-line-mode 1)
;;(global-flycheck-mode t)

(load-theme 'material-light t)
(set-face-attribute 'default nil :family "Droid Sans Mono" :height 105 :weight 'normal)

(setq make-backup-files nil)

(require 'ido
	 (ido-mode t))

;; Prevent accidentally killing emacs.
(global-set-key [(control x) (control c)]
  '(lambda ()
     (interactive)
     (if (y-or-n-p-with-timeout "Exit Emacs ? " 4 nil)
         (save-buffers-kill-emacs))))

;; enable autopairing
(require 'autopair
	 (autopair-global-mode)
	 (setq autopair-autowrap t))
(show-paren-mode t)
(setq show-paren-delay 1)

;; Emacs key bindings
;; ==================

(define-key global-map (kbd "RET") 'newline-and-indent)

(defun prev-window ()
   (interactive)
   (other-window -1))
(global-set-key (kbd "M-q") 'prev-window)

(global-set-key (kbd "<f1>") 'man-follow)
(global-set-key (kbd "<f2>") 'save-buffer)
(global-set-key (kbd "<f3>") 'goto-line)
(global-set-key (kbd "<f5>") 'find-makefile-compile)

(global-set-key (kbd "<f10>") 'switch-to-buffer)
(global-set-key (kbd "<f11>") 'previous-buffer)
(global-set-key (kbd "<f12>") 'next-buffer)

(global-set-key [M-left] 'tabbar-backward-tab)
(global-set-key [M-right] 'tabbar-forward-tab)
(global-set-key [M-down] 'tabbar-backward-group)
(global-set-key [M-up] 'tabbar-forward-group)

(global-set-key (kbd "M-z") 'kill-buffer-and-window)

(setq x-alt-keysym 'meta)

;; Emacs CUA mode C-v, C-c C-x
;; ===========================
(cua-mode t)
(setq cua-auto-tabify-rectangles nil)
(transient-mark-mode 1)
(setq cua-keep-region-after-copy t)


;; Tabber configuration
;; ====================
(require 'tabbar)
(tabbar-mode)
(setq tabbar-use-images nil)

;; Put tabs into groups
(defun tabbar-buffer-groups ()
  (list
   (cond
    ()
    ((member (buffer-name)
	     '("*scratch*" "*Messages*" "*Help*" "*Completions*" "*Shell Command Output*"))
     "Emacs")
    ((member (buffer-name)
	     '("TAGS" "*RTags Log*" "*RTags Diagnostics*" "*clang-output*" "*clang-error*"
	       "*Compile-Log*" "*markdown-output*" "*Flycheck error messages*"
		   "*epc con 3*" "*epc con 17*" "dart-analysis-server" "*dart-analysis-server*" "*dart-debug*"))
     "Hidden")
    ((member (buffer-name)
	     '("*terminal*"  "*compilation*" "*Gofmt Errors*" "*xref*"
	       "*Flycheck error messages*" "*Warnings*" "*Python*"))
     "Runtime")
    (t "User"))))

;; Tabbar face customization
(set-face-attribute
 'tabbar-default nil
 :background "gray80"
 :foreground "gray80"
 :height 0.9
 :box '(:line-width 1 :color "gray80" :style nil))
(set-face-attribute
 'tabbar-unselected nil
  :background "gray75"
  :foreground "gray20"
  :box '(:line-width 2 :color "gray70" :style nil))
(set-face-attribute
 'tabbar-selected nil
 :background "gray90"
 :foreground "black"
 :weight 'bold
 :box '(:line-width 1 :color "gray40" :style nil))
(set-face-attribute
 'tabbar-highlight nil
 :background "gray90"
 :foreground "grep20"
 :weight 'bold
 :underline nil
 :box '(:line-width 2 :color "gray60" :style nil))
(set-face-attribute
 'tabbar-modified nil
 :background "gray75"
 :foreground "gray40"
 :weight 'bold
 :underline nil
 :box '(:line-width 2 :color "gray60" :style nil))
(set-face-attribute
 'tabbar-selected-modified nil
 :background "gray90"
 :foreground "gray40"
 :weight 'bold
 :underline nil
 :box '(:line-width 2 :color "gray40" :style nil))
(set-face-attribute
 'tabbar-button nil
 :foreground "gray40"
 :box '(:line-width 2 :color "gray60" :style nil))
(set-face-attribute
 'tabbar-separator nil
 :background "gray60"
 :height 0.6)

;; Add a buffer modification state indicator in the tab label.
(defadvice tabbar-buffer-tab-label (after fixup_tab_label_space_and_flag activate)
  (setq ad-return-value
	(if (and (buffer-modified-p (tabbar-tab-value tab))
		 (buffer-file-name (tabbar-tab-value tab)))
	    (concat " + " (concat ad-return-value " "))
	  (concat " " (concat ad-return-value " ")))))

;; Called each time the modification state of the buffer changed.
(defun ztl-modification-state-change ()
  (tabbar-set-template tabbar-current-tabset nil)
  (tabbar-display-update))

;; First-change-hook is called BEFORE the change is made.
(defun ztl-on-buffer-modification ()
  (set-buffer-modified-p t)
  (ztl-modification-state-change))
(add-hook 'after-save-hook 'ztl-modification-state-change)

;; This doesn't work for revert, I don't know.
;;(add-hook 'after-revert-hook 'ztl-modification-state-change)
(add-hook 'first-change-hook 'ztl-on-buffer-modification)

;; Neotree customization
;; =====================
(require 'neotree)
(setq neo-smart-open nil)
(setq neo-theme 'ascii)
(setq neo-window-width 35)
(setq neo-window-fixed-size nil)
(global-set-key (kbd "<f8>") 'neotree-toggle)
(set-face-attribute 'neo-file-link-face nil :foreground "Grey50")
(set-face-attribute 'neo-dir-link-face nil :foreground "Grey40")
(set-face-attribute 'neo-header-face nil :foreground "Grey60")
(set-face-attribute 'neo-root-dir-face nil :foreground "Grey60")
(set-face-attribute 'neo-button-face  nil :foreground "DarkCyan")
(set-face-attribute 'neo-expand-btn-face nil :foreground "DarkCyan")

;; Emacs to get PATH environment
;; =============================
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (replace-regexp-in-string
                          "[ \t\n]*$"
                          ""
                          (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq eshell-path-env path-from-shell) ; for eshell users
    (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))

;; Term default behavior
;; =====================
; enable cua and transient mark modes in term-line-mode
(defadvice term-line-mode (after term-line-mode-fixes ())
  (set (make-local-variable 'cua-mode) t)
  (set (make-local-variable 'transient-mark-mode) t))
(ad-activate 'term-line-mode)

;; disable cua and transient mark modes in term-char-mode
(defadvice term-char-mode (after term-char-mode-fixes ())
  (set (make-local-variable 'cua-mode) nil)
  (set (make-local-variable 'transient-mark-mode) nil))
(ad-activate 'term-char-mode)

(add-hook 'term-mode-hook
   (lambda ()
     (define-key term-raw-map (kbd "M-x") 'nil)
     (define-key term-raw-map (kbd "M-q") 'prev-window)
     ))

;; Open compilation buffer with fixed size in vertical split
;; =========================================================
(setq compilation-window-height 10)

(defun windowsize-compilation-hook ()
  (when (not (get-buffer-window "*compilation*"))
    (save-selected-window
      (save-excursion
        (let* ((w (split-window-vertically))
               (h (window-height w)))
          (select-window w)
          (switch-to-buffer "*compilation*")
          (shrink-window (- h compilation-window-height))
	  ))))
  (when (not (get-buffer-window "*Gofmt Errors*"))
    (save-selected-window
      (save-excursion
        (let* ((w (with-selected-window))
	       (h (window-height w)))
          (select-window w)
	  (shrink-window (- h compilation-window-height))
          (switch-to-buffer-other-frame "*Gofmt Errors*")
	  ))))
  (when (not (get-buffer-window "*Python*"))
    (save-selected-window
      (save-excursion
        (let* ((w (with-selected-window))
	       (h (window-height w)))
          (select-window w)
          (switch-to-buffer "*Python*")
          (shrink-window (- h compilation-window-height))
	  ))))
  )
(add-hook 'compilation-mode-hook 'windowsize-compilation-hook)

;; Text mode default behavior
;; ==========================

(defun text-mode-setup-hook()
  (turn-on-auto-fill)
  (set-fill-column 80)
  (setq indent-tabs-mode nil )
  (setq tab-width 4 )
  )
(add-hook 'text-mode-hook 'text-mode-setup-hook)

;; Automatic mode selection
;; ========================

(defun add-to-mode (mode lst)
  (dolist (file lst)
    (add-to-list 'auto-mode-alist
                 (cons file mode))))

(add-to-mode 'c++-mode (list "\\.cpp$" "\\.cc$" "\\.hpp$" "\\.h$"))
(add-to-mode 'c-mode (list "\\.c$" "\\.h$"))
(add-to-mode 'go-mode (list "\\.go$"))
(add-to-mode 'dart-mode (list "\\.dart$"))
(add-to-mode 'python-mode (list "\\.py$"))
(add-to-mode 'markdown-mode (list "\\.markdown$" "\\.md$"))

;; C/C++
;; =====

(with-eval-after-load 'go-mode
  (require 'company)
  (require 'irony)
  (require 'company-irony)
  (require 'company-irony-c-headers)
  ;(require 'flycheck)
  ;(require 'flycheck-rtags)
  )

(defun c-cpp-mode-setup-hook()
  (irony-mode)
  (company-mode)
  (company-irony-setup-begin-commands)
  (flycheck-mode)

  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async)

  (setq company-backends (delete 'company-semantic company-backends))
  (eval-after-load 'company
    '(add-to-list 'company-backends '(company-irony-c-headers company-irony)))

  (setq company-idle-delay 0)
  (define-key c-mode-map [.] 'company-complete)
  (define-key c++-mode-map [.] 'company-complete)

  ;; Start RDM if not running
  ;;(add-hook 'find-file-hook 'rtags-start-process-maybe)

  ;(flycheck-select-checker 'rtags)
  ;(setq-local flycheck-highlighting-mode nil)
  ;(setq-local flycheck-check-syntax-automatically nil)

  (makefile-setup-hook)

  )
(add-hook 'c-mode-hook 'c-cpp-mode-setup-hook)
(add-hook 'c++-mode-hook 'c-cpp-mode-setup-hook)

;; GoLang enablement
;; =================

(with-eval-after-load 'go-mode
  (require 'company)
  (require 'company-go)
  (require 'flycheck)
  )

(defun go-mode-setup-hook()
  (require 'go-mode)

  ;; Enable flycehck in Go
  (flycheck-mode)

  ;; Enable company-mode
  (company-mode)
  (set (make-local-variable 'company-backends) '(company-go))
  (setq company-backends (delete 'company-semantic company-backends))
  (eval-after-load 'company
    '(add-to-list 'company-backends '(company-go)))

  ;; Call goimports before saving and then do gofmt
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook 'gofmt-before-save)

  (local-set-key (kbd "M-.") 'godef-jump)
  (local-set-key (kbd "M-,") 'pop-tag-mark)

  ;; Customize compile command to run go build
  (if (not (string-match "go" compile-command))
      (set (make-local-variable 'compile-command)
	   "go build -v"))
  (define-key (current-local-map) "\C-c\C-c" 'compile)

  ;; Tab behavior
  (setq tab-width 4 indent-tabs-mode 1)
  )
;(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
(add-hook 'go-mode-hook 'go-mode-setup-hook)


;; Python enablement
;; =================

(with-eval-after-load 'python-mode
  (require 'jedi)
  (require 'flycheck-pyflakes)
  (require 'company)
  (require 'company-jedi))

(defun python-mode-setup-hook ()
  (require 'python-mode)

  ;; python virtualenv
  (require 'virtualenvwrapper)
  (venv-initialize-interactive-shells)
  (setq venv-location "~/workspace/python-virtualenvs/")
  (add-hook 'venv-postmkvirtualenv-hook
 	    (lambda () (shell-command "pip install flake8 jedi")))

  ;; python indentation
  (setq-default indent-tabs-mode t)
  (setq-default tab-width 4)
  (setq-default py-indent-tabs-mode t)
  ;; remove whitespaces
  (add-hook 'before-save-hook 'delete-trailing-whitespace)

  (setq company-backends (delete 'company-semantic company-backends))
  (eval-after-load 'company
    '(add-to-list 'company-backends '(company-jedi)))

  (setq jedi:complete-on-dot t)

  (defun flycheck-python-setup ()
    (flycheck-mode))

  )
(add-hook 'python-mode-hook #'flycheck-python-setup)
(add-hook 'python-mode-hook 'jedi:setup)
(add-hook 'python-mode-hook 'python-mode-setup-hook)
;;(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))

;; Markdown mode configuration
;; ===========================

(defun markdown-mode-setup-hook ()
  (require 'markdown-mode)
  (gfm-mode)
  ;; live-preview C-x 3 and C-c C-c l
  ;; M-x markdown-other-window
  ;; M-x html-mode and M-x sgml-tags-invisible
  )
(add-hook 'markdown-mode-hook 'markdown-mode-setup-hook)

;; DartLang enablement
;; =================
;;(add-to-list 'auto-mode-alist '("\\.dart\\'" . dart-mode))
(with-eval-after-load 'dart-mode
  (require 'flycheck)
  (require 'dart-mode)
  )
(defun dart-mode-init ()

  (setq dart-format-path "dartfmt")
  (setq dart-executable-path "/opt/dart-sdk/bin/dart")
  (setq dart-analysis-server-snapshot-path "/opt/dart-sdk/bin/snapshots/analysis_server.dart.snapshot")
  (setq dart-debug t)
  (setq dart-enable-analysis-server t)

  (make-local-variable 'compile-command)
  (let* ((curfile (file-name-nondirectory (buffer-file-name)))
         (outfile (concat (substring curfile 0 -5) ".js")))
    ;;(setq compile-command (format "dart2js -o%s %s" outfile curfile))
    (setq compile-command (format "dart %s" curfile))
    )
  )
(add-hook 'dart-mode-hook 'dart-mode-init)
(add-hook 'dart-mode-hook 'flycheck-mode)
(add-hook 'dart-mode-hook 'dart-mode-init)

;; Ctags
;; =====

(setq path-to-ctags "/usr/bin/ctags")
(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f %sTAGS -e -R %s" path-to-ctags dir-name (directory-file-name dir-name)))
  )

;; Make
;; ====

(defun makefile-setup-hook()
  (defun parent-directory (dir)
    (unless (equal "/" dir)
      (file-name-directory (directory-file-name dir))))

  (defun find-file-in-hierarchy (current-dir pattern)
    (let ((filelist (directory-files current-dir 'absolute pattern))
	  (parent (parent-directory (expand-file-name current-dir))))
      (if (> (length filelist) 0)
	  (car filelist)
	(if parent
	    (find-file-in-hierarchy parent pattern)))))

  (defun find-makefile-compile ()
    (interactive)
    (let ((file (find-file-in-hierarchy
                 (file-name-directory buffer-file-name)
                 "^\\(GNUm\\|M\\|m\\)akefile$")))
      (if file
	  (let ((directory (file-name-directory file)))
	    (compile (format "cd \"%s\" && make" directory))))))

  (global-set-key (kbd "M-c") 'find-makefile-compile)
  (global-set-key (kbd "M-p") 'previous-error)
  (global-set-key (kbd "M-n") 'next-error)
  )
(makefile-setup-hook)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (material-theme use-package flymake-python-pyflakes flycheck-pyflakes flycheck-rtags rtags company-irony-c-headers company-irony irony company-jedi company-tern company-web company-go company markdown-mode dart-mode go-eldoc go-mode pony-mode virtualenvwrapper jedi epc python-environment exec-path-from-shell neotree tabbar autopair))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
